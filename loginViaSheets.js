import sheets from './sheets';

const spreadsheetId = process.env.SPREADSHEET_ID;
const usersRange = process.env.USERS_RANGE;

const getUserLogins = async () => {
  const gRes = await sheets.spreadsheets.values.batchGet({
    spreadsheetId,
    ranges: usersRange,
  });

  return Object.values(gRes.data.valueRanges[0].values);
}

export default async function checkLogin(user, {pw}) {
  const logins = await getUserLogins();

  const userRow = logins.find((row) => {
    return row[0] === user;
  });

  if (!userRow) {
    throw new Error(`Could not find that user.`);
  }

  if (userRow[0] === user && pw && userRow[1] === pw) {
    return true;
  }

  if (userRow[0] === user && !pw) {
    return true;
  }

  if (pw && userRow[1] !== pw) {
    throw new Error(`That password is incorrect.`);
  }

  throw new Error(`Could not log in. Incorrect username or password.`);
}
