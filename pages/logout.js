export default function Logout(props) {
  return <></>;
}

export async function getServerSideProps(context) {
  context.res.setHeader('Set-Cookie', [`user=false; Expires=-1 HttpOnly`]);
  context.res.setHeader('Location', ['/']);
  context.res.statusCode = 301;

  return { props: {} };
}

