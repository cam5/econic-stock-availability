import Head from 'next/head';
import Image from 'next/image'
import useSWR from 'swr';
import cookie from 'cookie';
import { useEffect } from 'react';
import styles from '../styles/Home.module.css';
import ProductCardList from '../components/productCardList';
import Login from '../components/login';
import loginViaSheets from '../loginViaSheets';

const fetcher = (...args) => fetch(...args).then(res => res.json());

// @TODO - also allow logout

export default function Home(props) {
  if (props.loggedIn === false) {
    return <Login {...props} />;
  }

  const { data, error } = useSWR('/api/inventory-data', fetcher);

  let content = <div>Loading...</div>;
  if (error) content = <div>Failed to load.</div>;
  if (data) content = <ProductCardList products={data} />;

  return (
    <div className={styles.container}>
      <Head>
        <title>Econic Apparel Stock Availability</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <a className="right-0 top-0 absolute pr-3 text-gray-400" href="/logout">Logout</a>

      <main className={styles.main}>
        <img src="/econic-logo.png" />
        <h1 className="text-2xl mt-5 mb-12">Econic Apparel Current Inventory Numbers</h1>
        {content}
      </main>
    </div>
  )
}

export async function getServerSideProps(context) {
  let user;
  let pw;

  if (context.req.headers.cookie) {
    const cookies = cookie.parse(context.req.headers.cookie);
    user = cookies.user;
    if (user === 'false') { user = false; }
    pw = cookies.pw;
  }

  if (context.query && context.query.u && context.query.pw) {
    user = context.query.u;
    pw = context.query.pw;

    if (pw === '') { pw = 'User submitted a blank password lol' }
  }

  if (!user && !pw) {
    return { props: { loggedIn: false } };
  }

  try {
    // Allow login without pass so long as we keep that user in the google
    // sheets, and they've logged in at least once.
    const loggedInUser = await loginViaSheets(user, {pw});

    if (loggedInUser) {
      if (context.query && context.query.u && context.query.pw) {
        context.res.setHeader('Location', ['/']);
        context.res.statusCode = 301;
      }

      context.res.setHeader('Set-Cookie', [`user=${user}; Expires=2147483647 GMT; HttpOnly`]);

      return { props: { loggedIn: true } };
    }

    throw new Error('Unexpected error logging in.');

  } catch (err) {
    let loginError = err;
    context.res.setHeader('Set-Cookie', [`user=false; Expires=-1 HttpOnly`]);

    return {
      props: {
        loggedIn: false,
        attemptedUser: context.query.u || null,
        loginFailMessage: String(err),
      }
    }
  }
}
