import sheets from '../../sheets';

const spreadsheetId = process.env.SPREADSHEET_ID;
const inventoryRange = process.env.SPREADSHEET_RANGE;

function extractBaseSKU(string) {
  const found = string.match(/\w+\d+/);
  return found ? found[0] : false;
}

/**
 * Given an array of the google sheet data, parse the rows out into
 * objects for each product.
 *
 * @param {array} sheetData - Array of arrays of strings representing
 *                            each cell's data.
 */
function gSheetToProducts(sheetData) {
  const products = {};
  const productNames = [];

  sheetData.forEach((row) => {
    if (row[0] === '' && row[1] === '' && row[2] !== '') {
      productNames.push(row[2]);
    }

    const baseSKU = extractBaseSKU(row[0]);

    const notesMentionMeToWe = !row[13] ? false : row[13].toLowerCase().match(/me to we/);

    if (baseSKU && !notesMentionMeToWe) {
      if (!products[baseSKU]) {
        products[baseSKU] = { colors: [] };
      }

      const matchedFullName = productNames.find((name) => name.match(baseSKU));

      if (matchedFullName) { products[baseSKU].name = matchedFullName; }

      products[baseSKU].sku = baseSKU;

      if (row[1] && Boolean(parseInt(row[10], 10))) {
        products[baseSKU].oneSize = true;
      }

      if (Boolean(row[1])) {
        products[baseSKU].colors.push(
          {
            [row[1]]: {
              'XS': parseInt(row[2], 10),
              'S': parseInt(row[3], 10),
              'M': parseInt(row[4], 10),
              'L': parseInt(row[5], 10),
              'XL': parseInt(row[6], 10),
              '2XL': parseInt(row[7], 10),
              '3XL': parseInt(row[8], 10),
              '4XL': parseInt(row[9], 10),
              'One Size': parseInt(row[10], 10),
              'Total': parseInt(row[11], 10),
            }
          }
        );
      }
    }
  });

  return Object.values(products)
    .filter((product) => product.colors.length > 0);
}

export default async (req, res) => {
  const gRes = await sheets.spreadsheets.values.batchGet({
    spreadsheetId,
    ranges: inventoryRange,
  });

  const sheetData = Object.values(gRes.data.valueRanges[0].values);

  res.statusCode = 200;
  res.json(gSheetToProducts(sheetData));
}
