import Product from './product';

function ProductCardList(props) {
  return (
    <>
      <div>{props.products.map(
        (product) => <Product key={product.sku} {...product} />
      )}</div>
    </>
  );
}

export default ProductCardList;
