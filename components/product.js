import slugify from 'slugify';

function Product(props) {
  return <div className={`${props.oneSize ? 'text-center ' : ''}my-10`}>
    <h2 className="bold uppercase my-6">{props.name || props.sku}</h2>
    <table className="mx-auto">
      <thead>
        {!props.oneSize && (
          <tr>
            <th className="text-right text-sm bold text-gray-500">COLOR</th>
            <td className="w-12 text-center text-sm bold text-gray-500">XS</td>
            <td className="w-12 text-center text-sm bold text-gray-500">S</td>
            <td className="w-12 text-center text-sm bold text-gray-500">M</td>
            <td className="w-12 text-center text-sm bold text-gray-500">L</td>
            <td className="w-12 text-center text-sm bold text-gray-500">XL</td>
            <td className="w-12 text-center text-sm bold text-gray-500">2XL</td>
            <td className="w-12 text-center text-sm bold text-gray-500">3XL</td>
            <td className="w-12 text-center text-sm bold text-gray-500">4XL</td>
            <td className="w-16 text-center uppercase text-sm">Total</td>
          </tr>
        )}

        {props.oneSize && (
          <tr>
            <th className="text-right text-sm bold text-gray-500">COLOR</th>
            <td className="w-12 text-center text-sm bold text-gray-500">QTY</td>
          </tr>
        )}
      </thead>
      <tbody>
        {props.colors.map((color) => {
          const colorName = Object.keys(color)[0];
          const sizes = Object.values(color)[0];

          if (props.oneSize) {
            return (
              <tr>
                <th className="border px-2 py-1 text-right text-sm" style={{ width: 200 }}>
                  <div className="inline-flex items-center">
                    {colorName}
                      <span
                        className={`bg-${slugify(colorName, {lower: true})} ${colorName === 'White' ? 'border' : ''} w-3 h-3 rounded-full inline-block ml-3`}>
                      </span>
                  </div>
                </th>
                <td className="border px-2 py-1">{sizes['One Size']}</td>
              </tr>
            );
          }

          return (
            <tr>
              <th className="border px-2 py-1 text-right text-sm" style={{ width: 200 }}>
                <div className="inline-flex items-center">
                  {colorName}
                    <span
                      className={`bg-${slugify(colorName, {lower: true})} ${colorName === 'White' ? 'border' : ''} w-3 h-3 rounded-full inline-block ml-3`}>
                    </span>
                </div>
              </th>
              <td className="border px-2 py-1">{sizes.XS}</td>
              <td className="border px-2 py-1">{sizes.S}</td>
              <td className="border px-2 py-1">{sizes.M}</td>
              <td className="border px-2 py-1">{sizes.L}</td>
              <td className="border px-2 py-1">{sizes.XL}</td>
              <td className="border px-2 py-1">{sizes['2XL']}</td>
              <td className="border px-2 py-1">{sizes['3XL']}</td>
              <td className="border px-2 py-1">{sizes['4XL']}</td>
              <td className="border px-2 py-1">{sizes.Total}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  </div>;
}

export default Product;

