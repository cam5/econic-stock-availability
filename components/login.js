import Head from 'next/head';

export default function Login(props) {
  return <div>
    <Head>
      <title>Login | Econic Apparel Stock Availability</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <main className="container mx-auto flex h-screen items-center justify-center text-center">
      <form className="rounded-md shadow-sm">
        <h1 className="my-12 text-2xl">Please Login</h1>

        {props.loginFailMessage && <div className="w-64 py-2 px-4 bg-red-100 text-red-600 text-center mx-auto my-4">{props.loginFailMessage}</div>}

        <div className="my-12">
          <div>
            <label htmlFor="username" className="text-left">Username</label>
            <input id="username" name="u" type="test" required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-teal-500 focus:border-teal-500 focus:z-10 sm:text-sm" placeholder="Username" />
          </div>
          <div className="my-4">
            <label htmlFor="password" className="text-left">Password</label>
            <input id="password" name="pw" type="password" autoComplete="current-password" required className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-teal-500 focus:border-teal-500 focus:z-10 sm:text-sm" placeholder="Password" />
          </div>
        </div>

        <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-teal-600 hover:bg-teal-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-teal-500">
          <span className="absolute left-0 inset-y-0 flex items-center pl-3">
            <svg className="h-5 w-5 text-teal-500 group-hover:text-teal-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
              <path fillRule="evenodd" d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z" clipRule="evenodd" />
            </svg>
          </span>
          Sign in
        </button>
      </form>
    </main>
  </div>
}
