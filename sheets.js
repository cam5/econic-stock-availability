import { google } from 'googleapis';
import { JWT } from 'google-auth-library';

const keysEnvVar = process.env.GOOGLE_SERVICE_ACCOUNT_CREDS;

if (!keysEnvVar) {
  throw new Error('The GOOGLE_SERVICE_ACCOUNT_CREDS environment variable was not found!');
}

const keys = JSON.parse(keysEnvVar);

const auth = new JWT({
  email: keys.client_email,
  key: keys.private_key,
  scopes: ['https://www.googleapis.com/auth/spreadsheets'],
});

const sheets = google.sheets({ version: 'v4', auth });

export default sheets;
